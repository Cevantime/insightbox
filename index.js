// on récupère express
const express = require('express');
const pug = require("pug");

// on crée notre application en appelant directement express
// comme une fonction
const app = express();

// on initialise un tableau d'insight (on n'utilise pas encore de bdd)
const insights = [
    {
        title: "Première idée",
        description: "Première description"
    },
    {
        title: "Deuxième idée",
        description: "Deuxième description"
    }
];

// permet de rendre accessible le répertoire public pour le navigateur
app.use(express.static('public'));

// permet de prévenir express que notre formulaire est envoyé dans le format classique (encodage URL)
app.use(express.urlencoded());

// nous définissons que lorsque l'adresse "/" sera visitée, 
// le callback situé en deuxième argument sera appelé
app.get('/', (req, res) => {
    // les insights sont passés au template en deuxième argument de la méthode renderFile
    res.end(pug.renderFile("templates/home.pug", { insights }));
});

app.post('/new', (req, res) => {
    insights.push(req.body); // req.body contient les données POST
    res.redirect("/");
});

app.listen(9000);